﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;



namespace Nosnosc
{
   public class Funkcje_kontrolek
    {
        //Wpisywanie tylko liczby 
        public static void Wpisywanie_Liczby(object sender, KeyPressEventArgs a)
        {
            if (a.KeyChar == '.')
                a.KeyChar = ',';

            if (a.KeyChar == ',' && (sender as TextBox).Text.Contains(','))
                a.Handled = true;

            if (a.KeyChar == ',' && (sender as TextBox).SelectionStart == 0)
                a.Handled = true;

            if (a.KeyChar == '-' && (sender as TextBox).SelectionStart != 0)
                a.Handled = true;


            if (char.IsDigit(a.KeyChar) || a.KeyChar == '-' || a.KeyChar == ',' || a.KeyChar == (char)Keys.Back)
            {         
            }
            else
                a.Handled = true;
        }

    }
}
